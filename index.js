

function init(){
    var canvas = document.getElementById('myCanvas');
    var ctx = canvas.getContext('2d');
    var pen_btn = document.getElementById('pen');
    var brush_btn = document.getElementById('brush');
    var eraser_btn = document.getElementById('eraser');
    var cir_btn = document.getElementById('circle');
    var cube_btn = document.getElementById('cube');
    var tri_btn = document.getElementById('tri');
    var A_btn = document.getElementById('A');
    var tool_now = pen_btn;
    var imgData = [];
    var now_data = 0;
    var save_color;
    
    imgData[0] = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
    pen_btn.style.backgroundImage = "url(img/sel.png)";
    A_btn.addEventListener('click', function() {
        ctx.strokeStyle = save_color;
        A_btn.style.backgroundImage = "url(img/sel.png)";
        pen_btn.style.backgroundImage = "";
        eraser_btn.style.backgroundImage = "";
        cir_btn.style.backgroundImage = "";
        cube_btn.style.backgroundImage = "";
        tri_btn.style.backgroundImage = "";
        brush_btn.style.backgroundImage = "";
        tool_now = pen_btn;
    });
    pen_btn.addEventListener('click', function() {
        ctx.strokeStyle = save_color;
        pen_btn.style.backgroundImage = "url(img/sel.png)";
        A_btn.style.backgroundImage = "";
        eraser_btn.style.backgroundImage = "";
        cir_btn.style.backgroundImage = "";
        cube_btn.style.backgroundImage = "";
        tri_btn.style.backgroundImage = "";
        brush_btn.style.backgroundImage = "";
        tool_now = pen_btn;
    });
    brush_btn.addEventListener('click', function() {
        ctx.strokeStyle = save_color;
        brush_btn.style.backgroundImage = "url(img/sel.png)";
        A_btn.style.backgroundImage = "";
        pen_btn.style.backgroundImage = "";
        eraser_btn.style.backgroundImage = "";
        cir_btn.style.backgroundImage = "";
        cube_btn.style.backgroundImage = "";
        tri_btn.style.backgroundImage = "";
        tool_now = brush_btn;
    });
    eraser_btn.addEventListener('click', function() {
        save_color = ctx.strokeStyle;
        ctx.strokeStyle = 'white';
        eraser_btn.style.backgroundImage = "url(img/sel.png)";
        A_btn.style.backgroundImage = "";
        pen_btn.style.backgroundImage = "";
        cir_btn.style.backgroundImage = "";
        cube_btn.style.backgroundImage = "";
        tri_btn.style.backgroundImage = "";
        brush_btn.style.backgroundImage = "";
        tool_now = eraser_btn;
    });
    cir_btn.addEventListener('click', function() {
        ctx.strokeStyle = save_color;
        cir_btn.style.backgroundImage = "url(img/sel.png)";
        A_btn.style.backgroundImage = "";
        eraser_btn.style.backgroundImage = "";
        pen_btn.style.backgroundImage = "";
        cube_btn.style.backgroundImage = "";
        tri_btn.style.backgroundImage = "";
        brush_btn.style.backgroundImage = "";
        tool_now = cir_btn;
    });
    cube_btn.addEventListener('click', function() {
        ctx.strokeStyle = save_color;
        cube_btn.style.backgroundImage = "url(img/sel.png)";
        A_btn.style.backgroundImage = "";
        eraser_btn.style.backgroundImage = "";
        cir_btn.style.backgroundImage = "";
        pen_btn.style.backgroundImage = "";
        tri_btn.style.backgroundImage = "";
        brush_btn.style.backgroundImage = "";
        tool_now = cube_btn;
    });
    tri_btn.addEventListener('click', function() {
        ctx.strokeStyle = save_color;
        tri_btn.style.backgroundImage = "url(img/sel.png)";
        A_btn.style.backgroundImage = "";
        eraser_btn.style.backgroundImage = "";
        cir_btn.style.backgroundImage = "";
        pen_btn.style.backgroundImage = "";
        cube_btn.style.backgroundImage = "";
        brush_btn.style.backgroundImage = "";
        tool_now = tri_btn;
    });


    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
    }
//tool
    function mouseMove(evt) {
        var mousePos = getMousePos(canvas, evt);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
    }
    var size=5;
    function brushMove(evt) {
        var mousePos = getMousePos(canvas, evt);
        ctx.arc(mousePos.x, mousePos.y, size, 0, 2 * Math.PI);
        ctx.fillStyle=ctx.strokeStyle;
        ctx.fill();
    }

    var xPos;
    var yPos;
    function mouseCircle(evt) {
        var mousePos = getMousePos(canvas, evt);
        ctx.arc(xPos, yPos, Math.sqrt( (xPos-mousePos.x)*(xPos-mousePos.x) + (yPos-mousePos.y)*(yPos-mousePos.y) ), 0, 2 * Math.PI);
        canvas_paste(now_data);
        ctx.stroke();
    }
    function mouseCube(evt) {
        var mousePos = getMousePos(canvas, evt);
        canvas_paste(now_data);
        ctx.strokeRect(xPos, yPos, -xPos+mousePos.x, -yPos+mousePos.y);
        ctx.stroke();
    }
    function mouseReg(evt) {
        var mousePos = getMousePos(canvas, evt);
        canvas_paste(now_data);
        ctx.moveTo((xPos+(mousePos.x-xPos)/2), yPos);//移到那一個坐標點 (X,Y)
        ctx.lineTo(mousePos.x, mousePos.y);//從x點到y點
        ctx.lineTo(xPos, mousePos.y);
        ctx.lineTo((xPos+(mousePos.x-xPos)/2), yPos);
        ctx.stroke();
        ctx.closePath();
    }
//listener
    canvas.addEventListener('mousedown', function(evt) {
        var mousePos = getMousePos(canvas, evt);
        if (tool_now == eraser_btn | tool_now == pen_btn){
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y);
            evt.preventDefault();
            canvas.addEventListener('mousemove', mouseMove, false);
        }
        else if (tool_now == brush_btn){
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y);
            evt.preventDefault();
            canvas.addEventListener('mousemove', brushMove, false);
        }
        else if (tool_now == cir_btn){
            xPos = mousePos.x;
            yPos = mousePos.y;
            ctx.beginPath();
            canvas.addEventListener('mousemove', mouseCircle, false);
        }
        else if (tool_now == cube_btn){
            xPos = mousePos.x;
            yPos = mousePos.y;
            ctx.beginPath();
            canvas.addEventListener('mousemove', mouseCube, false);
        }
        else if (tool_now == tri_btn){
            xPos = mousePos.x;
            yPos = mousePos.y;
            ctx.beginPath();
            canvas.addEventListener('mousemove', mouseReg, false);
        }
    });

    canvas.addEventListener('mouseup', function() {
        if (tool_now == eraser_btn | tool_now == pen_btn)
            canvas.removeEventListener('mousemove', mouseMove, false);
        else if (tool_now == brush_btn)
            canvas.removeEventListener('mousemove', brushMove, false);
        else if (tool_now == cir_btn) canvas.removeEventListener('mousemove', mouseCircle, false);
        else if (tool_now == cube_btn) canvas.removeEventListener('mousemove', mouseCube, false);
        else if (tool_now == tri_btn) canvas.removeEventListener('mousemove', mouseReg, false);
        canvas_copy();
    }, false);

//button
    document.getElementById('reset').addEventListener('click', function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvas_copy();
    }, false);

    document.getElementById('undo').addEventListener('click', function() {
        if (now_data==0) return;
        else canvas_paste(now_data-1);
        now_data = now_data-1;
    }, false);

    document.getElementById('redo').addEventListener('click', function() {
        if (imgData[now_data+1]) canvas_paste(now_data+1);
        else return;
        now_data = now_data+1;
    }, false);


//color
    var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black', 'white', 'ebebeb'];
    function listener(i) {
        document.getElementById(colors[i]).addEventListener('click', function() {
            if (tool_now!=eraser_btn)ctx.strokeStyle = colors[i];
        }, false);
    }
    for(var i = 0; i < colors.length; i++) {
        listener(i);
    }
//undo_redo
    function canvas_copy(){
        var width = ctx.canvas.width;
        var height = ctx.canvas.height;
        imgData.splice(now_data+1, imgData.length - now_data-1); 
        now_data = now_data + 1;
        imgData[now_data] = ctx.getImageData(0, 0, width, height);
    }
    
    function canvas_paste(now_data){
        ctx.putImageData(imgData[now_data],0,0);
    }
//font_size

    var size = [1, 5, 10, 20];
    var sizeNames = ['default', 'five', 'ten', 'twenty'];
    function fontSizes(i) {
        document.getElementById(sizeNames[i]).addEventListener('click', function() {
            ctx.lineWidth = size[i];
        }, false);
    }
    for(var i = 0; i < size.length; i++) {
        fontSizes(i);
    }
}






window.onload = function () {
    init();
};
